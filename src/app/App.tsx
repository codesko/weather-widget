import React from 'react';
import WeatherWidget from '../weather/WeatherWidget';
import styled from 'styled-components';

const Wrapper = styled.div`
    height: 100vh;
    width: 100vw;
    background: #eeeeee;
    display: flex;
    justify-content: center;
    align-items: center;
`;

const App = () => {
    return (
        <Wrapper>
            <WeatherWidget />
        </Wrapper>
    );
};

export default App;
