import React from 'react';
import useDarkSkyApi from './hooks/useDarkSkyApi';
import styled from 'styled-components';
import { CITIES } from './hooks/config';
import { City } from './hooks/interface';
import dateFns from 'date-fns';
import pl from 'date-fns/locale/pl';

// @ts-ignore
import WeatherIcon from 'react-icons-weather';

const Widget = styled.div`
    padding: 2em;
    width: 300px;
    background: white;
    border-radius: 30px;
    text-align: center;
    margin-top: 1em;
`;

const H1 = styled.h1``;

const H4 = styled.h4`
    color: #888888;
`;

const H5 = styled.h5`
    color: #888888;
`;

const IconWrapper = styled.div`
    i {
        font-size: 6em;
    }
`;

const CitySelect = styled.select`
    display: block;
    margin: 0 auto;
`;

const CitySelectOption = styled.option``;

const WeatherWidget = () => {
    const [weather, city, setCity] = useDarkSkyApi('Kraków');

    const handleChange = (event: any) => {
        const city: string = event.target.value;

        setCity(city);
    };

    return (
        <div>
            {weather && (
                <>
                    <CitySelect
                        onChange={(e: any) => handleChange(e)}
                        value={city}
                    >
                        {CITIES.map((item: City, index: number) => (
                            <CitySelectOption key={`city_option_${index}`}>
                                {item.name}
                            </CitySelectOption>
                        ))}
                    </CitySelect>
                    <Widget>
                        <IconWrapper>
                            <WeatherIcon
                                name="darksky"
                                iconId={weather.currently.icon}
                            />
                        </IconWrapper>
                        <H5>
                            {dateFns.format(new Date(), 'dddd, M MMMM', {
                                locale: pl,
                            })}
                        </H5>
                        <H1>
                            {Math.round(weather.currently.apparentTemperature)}
                            °C
                        </H1>
                        <H4>{weather.currently.summary}</H4>
                    </Widget>
                </>
            )}
        </div>
    );
};

export default WeatherWidget;
