import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { API, CITIES, DEFAULT_CITY } from './config';

const setResults = (key: string, response: any) => {
    window.localStorage.setItem(key, JSON.stringify(response));
    window.localStorage.setItem(
        `${key}+timestamp`,
        String(new Date().getTime())
    );
};

const getCachedResults: any = (key: string) => {
    return window.localStorage.getItem(key);
};

const isCacheValid = (key: string) => {
    const cachedTimestamp = Number(
        window.localStorage.getItem(`${key}+timestamp`)
    );
    const currentTimestamp = Number(new Date().getTime());
    const thirtyMinutes = 1000 * 60 * 30;

    if (currentTimestamp - cachedTimestamp < thirtyMinutes) {
        return true;
    }

    return false;
};

const useDarkSkyApi = (initialCityName: any) => {
    const [city, setCity] = useState(initialCityName || 'Warszawa');
    const [weather, setWeather] = useState();

    useEffect(() => {
        if (!isCacheValid(city)) {
            const getWeather = async () => {
                try {
                    const { latitude, longitude } =
                        CITIES.find(item => item.name === city) || DEFAULT_CITY;
                    // I applied a proxy, because there are CORS troubles in this domain.
                    const response = await axios.get(
                        `https://cors-anywhere.herokuapp.com/${API.FORECAST}/${latitude},${longitude}?lang=pl&units=auto`
                    );

                    if (response.status === 200) {
                        setResults(city, response.data);
                        setWeather(response.data);
                        setCity(city);
                    }
                } catch (error) {
                    // We should have the log error loging here.
                    console.log(error);
                }
            };

            getWeather();
        } else {
            setWeather(getCachedWeather());
        }
    }, [city]);

    const getCachedWeather: any = () => {
        return JSON.parse(getCachedResults(city));
    };

    return [weather, city, setCity];
};

export default useDarkSkyApi;
