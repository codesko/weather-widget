import { City } from './interface';

const secretKey = 'f8a76ccde05b3a7705ea2a7e41e48ac4';
const api = `https://api.darksky.net/forecast/${secretKey}`;

export const API = {
    FORECAST: api,
};

export const CITIES: City[] = [
    {
        name: 'Warszawa',
        latitude: '52.229675',
        longitude: '21.012230',
    },
    {
        name: 'Kraków',
        latitude: '50.049683',
        longitude: '19.944544',
    },
    {
        name: 'Gdańsk',
        latitude: '54.372158',
        longitude: '18.638306',
    },
    {
        name: 'Łódź',
        latitude: '51.759048',
        longitude: '19.458599',
    },
];

export const DEFAULT_CITY = CITIES[0];
